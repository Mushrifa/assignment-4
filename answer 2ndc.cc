#include <stdio.h>
//c programme to find the volume of a cone

int main()
{
	int height,radius;
	float pi,volume;
	
	printf("enter the height: \n");
	scanf("%d",&height);
	
	printf("enter the radius: \n");
	scanf("%d",&radius);
	
	pi=3.1429;
	volume=  pi * radius*radius * height/3;
	printf("volume of the cone is: %.3f \n",volume);
	
	return 0;
	
}
