#include <stdio.h>
//c programme to demonstrate bitwise operators
int main()
{
	int A=10 , B=15 , result;
	
	result=A&B;                                                // A   ----  1 0 1 0 =10 (AND)
	printf("result of A & B is : %d\n",result );              //B   -----   1 1 1 1 =15
	                           
							                                 // result--    1 0 1 0 =10 (output)
							   
	result=A^B;
	printf("result of A ^ B is : %d\n",result);             // A---   1 0 1 0 =10  (exclusive OR)
	                                                       // B ----  1 1 1 1 =15 
	                                               
												          // result   0 1 0 1 =5 (output)
												   
   result=~A;
   printf("result of ~A is : %d\n",result); 
   
   result=A<<3;
   printf("result of A<<3 is :%d\n",result);        // A -----  0 0 0 0 1 0 1 0   =10          
	                                                // A<<3 --  0 1 0 1 0 0 0 0   =80 (output)
	
   result=B>>3;
   printf("result of B>>3 is :%d\n",result);      // B ---    0 0 0 0 1 1 1 1 =15 												       
												 //B>>3--     0 0 0 0 0 0 0 1 =1 
												   
	return 0;
	
	
}
