#include <stdio.h>
// c programme to convert celsius into Fahrenheit

int main()
{
	float c,f;
	
	printf("Enter the temperature of celsius= \n");
	scanf("%f",&c);
	
	f=c*9/5+32;
	printf("Convert into Fahrenheit : %.2f \n",f);
	
	return 0;
}
